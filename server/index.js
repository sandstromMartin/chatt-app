const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const jwt = require('jsonwebtoken')
const{ addUser, removeUser, getUser, GetUsersInRoom, getUsersInRoom } = require('./users.js'); 


const PORT = process.env.PORT || 5000
const router = require('./router');
const { callbackify } = require('util');
const { Console } = require('console');
//create the server and sockt
const fs = require('fs')
const app = express();
const server = http.createServer(app);
const io = socketio(server, {
  handlePreflightRequest: (req, res)=>{
      const headers={
          "Access-Control-Allow-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Origin": req.headers.origin,
          "Access-Control-Allow-Credentials": true
      };
      res.writeHead(200, headers);
      res.end();
  }});

const privKey = fs.readFileSync('./keys/private.pem', 'utf-8')
const pubKey = fs.readFileSync('./keys/public.pem', 'utf-8')


    io.on('connect', (socket) => {

      console.log("Connected??")
      socket.on('join', ({ name, room }, callback) => {


        const { error, user } = addUser({ id: socket.id, name, room });
    
        if(error) return callback(error);
    
        socket.join(user.room);
    
        socket.emit('message', { user: 'admin', text: `${user.name}, welcome to room ${user.room}.`});
        socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `Oh no...${user.name} has joined us...!` });
    
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)})



        callback();
      });

      socket.on('username', username => {
         
         let token = jwt.sign(
          { 'username': username,
        }, privKey, { expiresIn: '180s' }
    );
      
      socket.emit('token', token);
      
  });
  
      

    socket.on('sendMessage',(message, callback) => {
        const user = getUser(socket.id);
        
        io.to(user.room).emit('message', { user: user.name, text: message});
        io.to(user.room).emit('roomData', { user: user.room, users: getUsersInRoom(user.room
          )});
        

        callback();
    });
  
    socket.on('disconnect',() => {
        const user = removeUser(socket.id);

        if(user){
          io.to(user.room).emit('message', {user: 'admin', text: `${user.name}, has left us...for now!`})

        }
        console.log('user disconnected!!!');

    });

     app.get('/public', verifyToken,(req,res) =>{
       res.json(pubKey)
   })

   app.post('/verify', verifyToken, (req,res)=>{
     const token = req.header('authorization')
     console.log(req.header)
  jwt.verify(token, privKey, {complete: true}, (error, authData)=>{
       if(error){
          
      }else{
        
        
        }
    })
    
 })

  });
 function verifyToken(req, res, next){
   //get auth header value
     const token = req.header('authorization')
    console.log("token: " + token)
    if(token) {
        next()
    }
     else {
         res.sendStatus(403)
     }

}

app.use(router);

//the port that server listening to
server.listen(PORT, () => console.log(`Server has started on port: ${PORT}`))